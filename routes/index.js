var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET pagina "Express". */
router.get('/express', function(req, res, next) {
  res.render('express', { title: 'Express' });
});

module.exports = router;
