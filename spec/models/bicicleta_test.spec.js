var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    /*beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error: '));
        
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        }); 
    });*/
    

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            //mongoose.disconnect(err);
            done();
        });
    });

    
    describe('Bicicleta.createInstance', ()=>{
        it('crea una instancia de bicicleta', ()=>{
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicleta.allBicis', ()=>{
        it('comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', ()=>{
        it('agrega solo una bici', (done) =>{
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done(); 
                });
            });
        });
    });

    describe('Bicicleta.findByCode', ()=>{
        it('Debe devolver la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color: 'rojo', modelo: 'montaña'});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });


    describe('Bicicleta.removeByCode', () => {
        it('Elimina la Bici con Code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
            
                var unaBici1 = new Bicicleta(
                    {code: 1,
                     color: 'Negra',
                     modelo: 'Mountain Bike'});
                Bicicleta.add(unaBici1, function(err, newBici) {
                    if (err) console.log(err);

                    var unaBici2 = new Bicicleta(
                        {code: 2,
                         color: 'Roja',
                         modelo: 'Clasica'});
                    Bicicleta.add(unaBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.removeByCode(1, function(err, targetBici){
                            if (err) console.log(err);
                            Bicicleta.allBicis(function(err, finalBicis){
                                expect(finalBicis.length).toBe(1);
                                expect(finalBicis[0].code).toEqual(unaBici2.code);
                                expect(finalBicis[0].color).toEqual(unaBici2.color);
                                expect(finalBicis[0].modelo).toEqual(unaBici2.modelo);
        
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});




/*
beforeEach(()=>{
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', ()=>{
    it('comienza vacia', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});
 
describe('Bicicleta.add', ()=>{
    it('agregamos una', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012434,-58.3861497]);

        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', ()=>{
    it('Debe devolver la bici con ID 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici1 = new Bicicleta(1, 'rojo', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'urbana');

        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    });
});

//HACER EL REMOVEBYID
*/
